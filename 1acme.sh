#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1acme.sh' encrypted at Fri Nov 29 03:53:47 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SY�ɭ �_������߯���������������CA@B���
(��#� P    �M  gAB@%��24L�M�OMLFT�1S�Fڌ����S54��=m!�CM��� 4@�Ѧ�24d44�M �2h�ɓ#L�L�  a0�&##L�0�!� hh�M�2`&�`�4hdɑ�F&F� 0�C��@M��B)��224d�@��M���h4   �  ��*H'���z�ѵG���Ѡ� �         P   $F� @�L�# &iO�e1��=2M����<���Th=L��=F��z�S�F��<� �¢*��e�ow������ī㍓�>�R�M`��Y�1�8�t=)2{���A���#��9�GC�C&l�m��S0�?��WF��\�0�����l#L9�5_��FP]��£ �c?Y�q������'߉�u\r*5��r�� /�̎��J��[zh�m����J9���:ͳ�q����sf��X�!H:}v�w!��/M��h��m�{i��cl�"��S5ꞠxW�04c�{O[�!AwL����H[5��"��YK��Zы��d�ȲY�_	a�������3��he̦2V!�ո��vD0Ҥ(M�6�-"P�l.km��]JE�&�U-�`�t�P�©�?w�����qp�+�dw��"���L�8��e��@@��ѫ�M�@���&N@�D���J$�<п8�A�.�%?�@2fu#�n�{HW䀜�f����Ϝ��g=^(=��F�F�i��?���e
�����R�+UY��PV��?2�b8<��nO���t�&L	�K�-֔<Q8y[���
��`wH����ѳ��fV`A�X�=����3|o��h�c7ؕ�!0Ty�:��H.��;lD�4��m2���Cp~;�i��n[�n!'�2�*H�r�w�}�v�0�k�,�n�*�0Qϔ6��9� G0>���n#���4�hCb��G%C+��ɇu��($R�-���R�3�K7e��DL��֟͌�O/��oGGG�У[��"�Z�r8���υW�i�2q��h�h��w��ug�4Cm��%*`xC���V|NH-JK�L�$�z)���r����G����������c���l�M�n�?�!��7�D/u!����UTL�t,4�9�_��C������g��:����CI�N6OcY��.~�'�4"�b��/���B�yau�u�K�ٳ'����W��q�ϏQ�͜o��;���
a�c��R�<��s߿=�1J�Q/�d�R@�ngV�\�D�ـ	n.T���X�Yi�,D�I6!��2�ߗ��.����N�UX��4t�H�1 #kz��
�N&�P��D#�9l���I�!4����s`D@��t��L��5����,)!�nLD�LE6��ne�ʔ��/P�.�5�`V���N��>��G����f�n�B|0�=a5hM�1��U鎡�2Ϧ��ZU�� ; X��O�Ϳ[��y��������1��R�(QI b`">t��PIH@�M�� +B۬UG��\��IM��i>g_O��������a�'��`0Y��Ghn�V{f��d;>� ���Ⳛ�9�5K�!��h�ۻ�dql������Ur]A�`����6pp� W�]kCM�c�hQЀ$\�����""\��25xa��a���}�LqϜ�~�%�w&��P;|�y�M�fct!ٗ��\'��i�&�����- Y(�G�=�X��k����Í4�x܀1����e148��n�^ ��.`q2�p$�}%�QЮ2�U.0�K��[���2�%ee�\&S�0�����<�:��ٍ��~��"R�]P��%����L�ݕ��_�$�h��@I&"������x"#ߧM���L��{��I���U�_e��%Vq�������}�~ ��f�C1=qv�a�S��C>�Z햴�H�����VY,:����OH����9��߽GĄx꽏���]w�I�X#����h�0K��*&X���1'��0�H����dW�2���K ��Mn�Ҭ}�l>ɐ;kj(0:����_�3����e~�$]#����|��u�>�:G�\��Ta�ׯ_
W|T�0�˨���o��JtP���& 0M�y�8��iM �4��jX!�G+,��򲳞��T�2���h�1ߛ���ޤ��dT�yB}�g0=�~���{@�_�f0ξ" ��1>3D&�3���I#���~+�@4C #�J��EX�/�����p& �Gb��p̈́"-�v$��B ��E���Ւ�^�PWn��0-
��4�	*�D��G���1���U�nÑ�s�-\}]����?»��d�=⸸��E�p��=���i��ِ|�&�&ka�*��d�� g
R�-�ȵ�{P�_hMT.W!�h�_��r�?P�}�S��0����*��f�eG�5���T[�B�i��/i�l��%��Q�`V��u�!fe���K0&�T��H�2��������Կk���i3 �\ɝ�؍��/��:PYP�e�Ѭ��G���g2�GĴ	lT8�u��ZP*Z��������U�A*��$x3�����&�y0%c�e�A�Fo���.&�G,�RCz�:�l�fi��Fa*WM��9\h����P�����e�S:
��P�z�s�PCJ� �4�*@H-Ai�"Z��i���}�F��>�d���8d`ؚ\�}4��Yb��wJ�k:��a s@(�A@��RH�_k�f� E�js��폣� a��b�ʽ0�`N`�"�!^�q�_�8g�`��~u�a�jcB���b(4PÜ�r$#	6 ���T�HIP ��2�QJH?TIwgF��(G@�B�i	y�1[��c`-�BM&bi�Ě�]�)��b1	�Ƶ��e>f{��ֱ^%�3oК�Aڻ2�~�K���Fb�U�:�� �a��!B�!�4nJ���V�rͤ$��V��X��Q�'B\5$I1D&H��MPЅ��t��ɔ�,��I��&��CI���`�$�ve(�Ty�3	+��1��;wn'x���Ự���H�a�����)�(� ��i���??��\��Z�Β��%�<�<�@�g��L�e�vF	+��'+^ԓ(��Phm�"E�d� ��ȣ��	͍,y|�QD�$�ұ_%��E��a��E�CNp�r�UM�(V�h&&P�bS<EZ�bFm�p��B��^X��!���E�*�QAfƳJrձ
O$$�L��m"�g%؀��pI��Y�e-Ǣ�!&����lm��j�bL�RhKA�7G ��f�Y����<�.{���z��(���XZ$�&��/���Z�
�oA������) ��b́S=jl�!��֐�2MHb�1�Mbʒ]!�p2�,+#9b���<�w��И�^�R�9Ң�� #�y� [�U�2�D%1H����&��3�0fb�Br7
R���q��+�,Z�,���YhQZ
�`��pY��i
��HBb�
������\����p��g�[��x�yhÜECR��]�_-��g�ٟ"�6Y"M-%�a��,�J�P)����Kb��L�j�(���`����,B�K��LAeLM����	)��-�☈���2���!�~ (��Ѓ��Vԑfm&�l���w�[	LF�`	��a���"�����ևU�S7���S��#o,�zT�7)I6 �r �F��d�L���`.ađn+Dxb/W��%1��"��0��a`�fs�% �.R�`[\O�����=/�AVZ����F��1/Ln���{L�����E�T���?w��Ͷ�aP,%E+� �)�Qt@T�E��H��l�����:���g���17�op�-�_#M1��D�0w�QCl7bOe���6KS�����⠧W���"��������|�H�����3D;$26�A�!�1��J�4�J<�Ċ �L�` ���0J�$S�]d���G�Z]2��!�J_�0,�Q�ȿk� ڲ�Q	HHRG쳁��RH5��v�f�	e�D���3��m�2��,���)̘�p4�?�ː�\�\X�*4��4j:!$2���JR�M�pq,�IL�A�f�y$����d���
�iN�ۃ\�Ri,�� �� D0��±!/�R��@��t���YE�n��}i����u��䃙PS3�TD �.;Ym3��S����B����g~��%	PmH$t�ԭ�8�.�.�-|�ĄV��Y�>��HJ�4�`r���9�I$�w���1���#�eI"�O��I�֦6:P�Џ�moq	n[Q�_��<�8��N��8�PG�
�H���C<����>A�T����_G�b�5�)��`���Iy,V�i	/ ��t!��o�mv&�6�`�d���<�^��zGs�$,��Z��Z���U�Ǖ�����bH��B�����x-$���$���X��:?dox���m/(���9=j�U���8�5#DoUmh��L`�jD�{�.:�ṫx5��S���(��5Z��g-T��G3$���b-L\(�н�u��'�X�I�b����1�	��!�b��P ��I�����Z� �����]+�ij-C�/h;Z�x�كm� � �0"hlPcM�cc(:/e	jSQ���8�#P�I.��ZH��5ך�ʶ)���.-���!��x���v�g�C�mv����Ʋ�c���g���f!U�χ�zF��\�1D&�|��o'v�z���ij�n��Cw9	u�Y�~u��ՙ�K􏞀X!+�mLy��n%ZNu*V5����Q:H	����	I���=�*�.�eJ��K�i7�0h��K�r� k���< �
EX�D�v9K�ͪ�^_��Z�I_R�n��F��˹�Q0]��-�z�ߔ{��Ճ��o�0�g���Ӯ�7pϡ������3���!�N�v�z�T$����30�3����I��D�x$4��%}���ހTb���2hf�ر�����e�*��ֹkiRfl�nIԮ!<��A�C�L��Su����m׃����0%�T���w2wV��ЍeKoQ�&��	�$E��D��H�A����㣇)�k����{<��[�x���4���G��^��D��P:��9y�!Đ�s��>i��r�K��G���ti�\c1��W
�u��$Q�אW��(L�6]2xOpx����Xѝ!(�d9�TK
ښ�JIp�ϧ'"d��N�p�r�}�,�=��I��C�k��㳀W+|hQ�W�n���7���Hb��=p�Yq]텞��T�g����U��ݖ�g��윾 g�6Y�Ha�F��I@���+P3�I훆~�"E>r��¡�NhL���.`�G�"�^ԧ���5����&�#���X:�L%��a�-{'�c٣Zv�9���ZD,���J�E���{����ǡ�C�"~_g�G�4�2V�;%�T�#S���N��
@�*�Ndo�Y}�"n�O	#�6�����)�h�Mh